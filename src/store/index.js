import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userInfo: [],
    showError: false
  },
  mutations: {
    mutationsUserInfo(state, payload) {
      state.userInfo = payload;
    },
    mutationsShoeError(state, error) {
      state.showError = error;
    }
  },
  actions: {
    getUser({ commit }, userName) {
      commit("mutationsShoeError", false);
      axios
        .get(`https://api.github.com/users/${userName}/repos`)
        .then(res => {
          commit("mutationsUserInfo", res.data);
        })
        .catch(() => {
          commit("mutationsShoeError", true);
        });
    }
  },
  getters: {
    getUserInfo(state) {
      return state.userInfo;
    },
    getShowError(state) {
      return state.showError;
    }
  }
});
